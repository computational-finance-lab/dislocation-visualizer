Dislocation Visualizer

---

This repository contains data and source code to create high resolution 
(60 second frequency) interactive visualizations of dislocation segment occurrence
frequencies in calendar year 2016 for the constituents of the Dow 30.

`data/` contains CSV files with the relative frequency of dislocation segment occurrences by minute.

`scripts/ts_viewer.py` can take one or more paths to CSV files and generate interactive
HTML figures for each.
These interactive HTML visualizations are created using [plotly](https://github.com/plotly/plotly.py).

`app/app.py` will open an interactive dashboard that fills a similar role to the
HTML visualizations discussed above, but allows multiple time series to be placed
on the same axes and has several other usability improvements.
This dashboard is created using [plotly Dash](https://dash.plot.ly/).

The python source code in this repository was tested with version 3.6, and may not
function correctly if a different version of python is used.
Additional python dependencies are listed in `requirements.txt`.

`scripts/generate_htmls/sh` uses [GNU Parallel](https://www.gnu.org/software/parallel/)
to accelerate the creation of multiple independent HTML visualizations.
