import argparse
import webbrowser
from pathlib import Path

import pandas as pd
import plotly as py
import plotly.express as px


def get_parser():
    parser = argparse.ArgumentParser(
        description='Lazily generates and shows an interactive time series visualization.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        'data_file',
        help='A valid file name that points to a CSV file containing one or more time series to be plotted.',
    )

    parser.add_argument(
        '--force',
        action='store_true',
        help='Force the generation of html files, even if they already exist.',
    )
    parser.add_argument(
        '--show',
        action='store_true',
        help='Opens the generated html file in a new browser tab.',
    )

    return parser


def main(*, data_file, show, force):
    out_path = Path('../html') / Path(data_file).name
    out_path = out_path.with_suffix('.html')

    if force or not out_path.exists():
        name = Path(data_file).stem.replace('start_hist_no_agg_', '').replace('_normed', '').replace('_', ' ')
        ticker = name.split()[0]
        df = pd.read_csv(data_file, header=None, index_col=0, parse_dates=True)
        df.columns = [name]
        df.index.name = 'Timestamp'
        df = df.reset_index()

        fig = px.line(
            df,
            'Timestamp',
            name,
        )

        # Edit the layout
        fig.update_layout(
            title=f'{ticker} Dislocation Occurrence Distribution',
            xaxis_title='Time',
            yaxis_title='Relative Frequency',
            xaxis_rangeslider_visible=True,
        )

        out_path.parent.mkdir(parents=True, exist_ok=True)
        py.io.write_html(fig, str(out_path))

    if show:
        webbrowser.open_new_tab(f'file://{str(out_path.absolute())}')


if __name__ == '__main__':
    args = get_parser().parse_args()

    main(**vars(args))
