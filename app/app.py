"""
Adapted from the dash example shown here:
    https://github.com/plotly/simple-example-chart-apps/tree/master/dash-timeseriesplot
"""

from pathlib import Path

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from dash.dependencies import Input, Output


def load_data():
    data_files = sorted(Path('../data').glob('*.csv'))

    dfs = []
    for data_file in data_files:
        name = Path(data_file).stem.replace('start_hist_no_agg_', '').replace('_normed', '').replace('_', ' ')
        df = pd.read_csv(data_file, header=None, index_col=0, parse_dates=True)
        df.columns = [name]
        df.index.name = 'Timestamp'
        dfs.append(df)
    return pd.concat(dfs, axis=1)


df = load_data()

app = dash.Dash(__name__)

div_style = {
    'width': '100%',
    'display': 'flex',
    'align-items': 'center',
    'justify-content': 'center',
}
app.layout = html.Div([
    html.H1("Dislocation Occurrence Distributions", style={'textAlign': 'center'}),
    dcc.Dropdown(
        id='my-dropdown',
        options=[{'label': x, 'value': x} for x in df.columns],
        multi=True,
        value=[df.columns[0]],
        style={
            "display": "block",
            "margin-left": "auto",
            "margin-right": "auto",
            "width": "60%",
        }
    ),
    html.Div([dcc.Graph(id='my-graph')], style=div_style)],
    className="container",
)


@app.callback(Output('my-graph', 'figure'), [Input('my-dropdown', 'value')])
def update_graph(selected_dropdown_values):
    traces = [[
        go.Scatter(
            x=df.index,
            y=df[x],
            mode='lines',
            opacity=0.7,
            name=x,
            textposition='bottom center',
        )
        for x in selected_dropdown_values
    ]]

    data = [val for sublist in traces for val in sublist]
    figure = {
        'data': data,
        'layout': go.Layout(
            colorway=["#5E0DAC", '#FF4F00', '#375CB1', '#FF7400', '#FFF400', '#FF0056'],
            height=720,
            width=1280,
            autosize=True,
            title=f"Dislocation Occurrence Distribution for {', '.join(str(x) for x in selected_dropdown_values)}",
            xaxis={
                "title": "Time",
                'rangeselector': {'buttons': list([
                    {'count': 8, 'label': '1T', 'step': 'hour', 'stepmode': 'backward'},
                    {'count': 1, 'label': '1D', 'step': 'day', 'stepmode': 'backward'},
                    {'count': 7, 'label': '1W', 'step': 'day', 'stepmode': 'backward'},
                    {'count': 1, 'label': '1M', 'step': 'month', 'stepmode': 'backward'},
                    {'count': 6, 'label': '6M', 'step': 'month', 'stepmode': 'backward'},
                    {'count': 1, 'label': '1Y', 'step': 'year', 'stepmode': 'backward'},
                    {'step': 'all'}
                ])},
                'rangeslider': {'visible': True},
                'type': 'date'},
            yaxis={"title": "Relative Frequency"},),
    }
    return figure


server = app.server


if __name__ == '__main__':
    app.run_server(debug=True)
